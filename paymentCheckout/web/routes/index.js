var express = require('express');
var request = require('request-promise');

var router = express.Router();
var status = 0;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Checkout de Pagamento' });
});

router.post('/', function(req, res, next) {
  console.log("Dados do pagamento validados:\n" + JSON.stringify(req.body));

  var options = {
    method: 'POST',
    uri: 'http://localhost:8080/pagamentos',
    body: req.body,
    json: true,
    headers: {
      'Content-Type': 'application/json',
      }
  }

  var result = "Pagamento não realizado";

  request(options).then(function (response){
    console.log("Pagamento registrado via API: " + response);
    if(response == true){
      result = 'Pagamento registrado com sucesso!';
    }else if(isNaN(result) === true){
      result = 'Boleto para pagamento: ' + response;
    }
    res.render('result', { title: result});
  }).catch(function (err) {
    console.log(err);
    res.render('result', { title: result });
  })
});

module.exports = router;
console.log("Started app");