function checkRange(codeAsStr, digits, start, end){
    if(codeAsStr.length < digits){
        return false;
    }
    var code = codeAsStr.substring(0, digits);
    var value = parseInt(code);
    return (value >= start && value <= end);
}

function isMasterCard(cardNumberStr){
    /* 51-55 ou 2221–2720 */
    return (checkRange(cardNumberStr, 2, 51, 55) === true
            || checkRange(cardNumberStr, 4, 2221, 2720) === true);
}

function isVisa(cardNumberStr){
    /* 4 */
    return cardNumberStr.startsWith('4');
}

function isAmex(cardNumberStr){
    /* 34-37 */
    return (checkRange(cardNumberStr, 2, 34, 37) === true);
}

function checkQtDigits(qtDigits, size){
    for(var i=0; i < qtDigits; i++){
        if(qtDigits[i] = size){
            return true;
        }
    }
    return false;
}

function checkCardNumber(brand, cardNumber, qtDigits){
    var size = cardNumber.length;
    if(checkQtDigits(qtDigits, size) === false){
        alert(brand + ': Quantidade de dígitos inválida.');
        return;
    }

    var sum = 0;
    var doubling = false;
    for(var c = size - 1; c >= 0; c--){
        var digit = parseInt(cardNumber.charAt(c));
        if(doubling === true){
            digit = digit * 2;
            if(digit > 9){
                digit -= 9;
            }
        }

        sum += digit;
        doubling = !doubling;
    }

    if((sum % 10) === 0){
        document.getElementById('brand').innerHTML= ' ' + brand;
    }else{
        alert(brand + ': Número inválido de cartão.');
    }
}

function checkBrand(){
    var cardNumber = document.getElementById('ccnumber').value;
    if(!cardNumber){
        document.getElementById('brand').innerHTML= ' ';
        return;
    }

    if(isNaN(cardNumber)){
        document.getElementById('brand').innerHTML= ' Inválido';
        return;
    }

    if(isVisa(cardNumber) === true){
        checkCardNumber('Visa', cardNumber, [13, 16, 19]);
        return;
    }

    if(isAmex(cardNumber) === true){
        checkCardNumber('AmericanExpress', cardNumber, [15]);
        return;
    }

    if(isMasterCard(cardNumber) === true){
        checkCardNumber('MasterCard', cardNumber, [16]);
        return;
    }

    document.getElementById('brand').innerHTML= ' Inválido';
}