function checkEmptyFields(fields){
    for(var i=0; i < fields.length; i++){
        if (!fields[i].value){
            fields[i].focus();
            alert('Necessário preencher todos os campos.');
            return false;
        }
    }
    return true;
}

function checkCommonFields(){
    var fields = document.getElementsByClassName('required');
    return checkEmptyFields(fields);
}

function checkPaymentFields(){
    var types = document.getElementsByName('paymentType');
    if(types[0].checked === false && types[1].checked === false){
        alert('Necessário tipo de pagamento.');
        return false;
    }
    return true;
}

function checkCardFields(){
    if(document.getElementById('boleto').checked){
        return true;
    }
    var cardFields = document.getElementsByClassName('cardreq');
    if(checkEmptyFields(cardFields) === false){
        return false;
    }

    var today = new Date();
    var expiration = document.getElementById('ccexpiration');
    if((new Date(expiration.value)) < today){
        expiration.focus();
        alert('Este cartão já está vencido.');
        return false;
    }
    return true;
}

function checkNumberFields(){
    var values = document.getElementsByClassName('value');
    for(var i=0; i < values.length; i++){
        var value = values[i].value;
        if (value && (isNaN(value) || value <= 0)){
            values[i].focus();
            alert('Campo numérico inválido.');
            return false;
        }
    }
    return true;
}

function check(){
    var ok = checkCommonFields() 
            && checkPaymentFields() && checkCardFields()
            && checkNumberFields();
    if(ok === true){
        var amount = document.getElementById('amount').value;
        document.getElementById('amount').value = amount * 100;
    }

    return ok;
}