FROM node:4.2.6
COPY ./web /var/www
WORKDIR /var/www
RUN npm install
RUN npm install request --save
ENTRYPOINT npm start
EXPOSE 3000