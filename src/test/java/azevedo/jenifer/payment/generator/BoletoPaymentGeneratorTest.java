package azevedo.jenifer.payment.generator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import azevedo.jenifer.payment.model.Payment;
import azevedo.jenifer.payment.model.PaymentRecord;
import azevedo.jenifer.payment.model.PaymentType;
import azevedo.jenifer.payment.utils.PaymentDataBuilder;

/**
 * Testes para geração de pagamento via boleto.
 * 
 * @author jenifer
 */
public class BoletoPaymentGeneratorTest extends PaymentGeneratorTest {

	/**
	 * Verifica pagamento com todos os dados corretos.
	 */
	@Test
	public void testSuccessBoletoPayment() {
		PaymentRecord paymentRecord = PaymentGenerator.generate(buildSuccess());
		assertSuccessTest(BoletoPaymentGenerator.OK_MSG, paymentRecord);

		try {
			Long boletoNumber = Long.valueOf(paymentRecord.getStatus());
			assertNotNull(boletoNumber);
			assertTrue(boletoNumber > 0L);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Verifica se pagamento é considerado inválido.</br>
	 * Pagamento com cliente inválido não pode ser efetuado com sucesso e deve
	 * conter mensagem apontando erro no cliente.
	 */
	@Test
	public void testInvalidClientBoletoPayment() {
		PaymentRecord paymentRecord = PaymentGenerator
				.generate(buildFailInvalidClient());
		assertFailTest(paymentRecord, "Cliente");
	}

	/**
	 * Verifica se pagamento é considerado inválido.</br>
	 * Pagamento com comprador inválido não pode ser efetuado com sucesso e deve
	 * conter mensagem apontando erro no comprador.
	 */
	@Test
	public void testInvalidBuyerBoletoPayment() {
		PaymentRecord paymentRecord = PaymentGenerator
				.generate(buildFailInvalidBuyer());
		assertFailTest(paymentRecord, "Comprador");
	}

	/**
	 * Verifica se pagamento é considerado inválido.</br>
	 * Pagamento com dados de pagamento inválido não pode ser efetuado com
	 * sucesso e deve conter mensagem apontando erro no pagamento.
	 */
	@Test
	public void testInvalidPaymentBoletoPayment() {
		PaymentRecord paymentRecord = PaymentGenerator
				.generate(buildFailInvalidPayment());
		assertFailTest(paymentRecord, "Pagamento");
	}

	/**
	 * Auxiliar cria pagamento com dados válidos.
	 */
	@Override
	protected Payment createValidPayment() {
		return new PaymentDataBuilder()
				.buildPayment(10000L, PaymentType.BOLETO, null).getPayment();
	}

	/**
	 * Auxiliar cria pagamento com dados inválidos.
	 */
	@Override
	protected Payment createInvalidPayment() {
		return new PaymentDataBuilder()
				.buildPayment(null, PaymentType.BOLETO, null).getPayment();
	}

	@Override
	protected void assertFailStatus(String status) {
		assertNull(status);
	}
}
