package azevedo.jenifer.payment.generator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import azevedo.jenifer.payment.model.Buyer;
import azevedo.jenifer.payment.model.Card;
import azevedo.jenifer.payment.model.Client;
import azevedo.jenifer.payment.model.Payment;
import azevedo.jenifer.payment.model.PaymentRecord;
import azevedo.jenifer.payment.utils.PaymentDataBuilder;

/**
 * Base para testes da geração de pagamentos.
 * 
 * @author jenifer
 */
public abstract class PaymentGeneratorTest {

	private static final String BUYER_NAME = "Buyer Name Test";

	private static final long BUYER_CPF = 1111111111L;

	private static final String BUYER_EMAIL = "buyer.name@test.com";

	private static final long DAY = 1000 * 60 * 60 * 24;

	private static final Date NEXT_DAY = new Date(new Date().getTime() + DAY);

	/**
	 * Cria cenário com todos os dados do pagamento corretos.
	 * 
	 * @return Container com os dados do pagamento.
	 */
	protected PaymentRecord buildSuccess() {
		return buildBaseScenario().getPaymentRecord();
	}

	protected PaymentRecord buildFailInvalidClient() {
		return buildBaseScenario().setClient(createInvalidClient())
				.getPaymentRecord();
	}

	protected PaymentRecord buildFailInvalidBuyer() {
		return buildBaseScenario().setBuyer(createInvalidBuyer())
				.getPaymentRecord();
	}

	protected PaymentRecord buildFailInvalidPayment() {
		return buildBaseScenario().setPayment(createInvalidPayment())
				.getPaymentRecord();
	}

	protected PaymentDataBuilder buildBaseScenario() {
		return new PaymentDataBuilder().setClient(createValidClient())
				.setBuyer(createValidBuyer()).setPayment(createValidPayment());
	}

	/**
	 * @return cliente válido.
	 */
	private Client createValidClient() {
		return new PaymentDataBuilder().buildClient("djw394dcs3").getClient();
	}

	/**
	 * @return cliente com id nulo.
	 */
	private Client createInvalidClient() {
		return new PaymentDataBuilder().buildClient(null).getClient();
	}

	/**
	 * @return comprador válido.
	 */
	private Buyer createValidBuyer() {
		return new PaymentDataBuilder()
				.buildBuyer(BUYER_NAME, BUYER_EMAIL, BUYER_CPF).getBuyer();
	}

	/**
	 * @return comprador com nome nulo.
	 */
	private Buyer createInvalidBuyer() {
		return new PaymentDataBuilder().buildBuyer(null, BUYER_EMAIL, BUYER_CPF)
				.getBuyer();
	}

	/**
	 * @return cartão válido.
	 */
	protected Card createValidCard() {
		return new PaymentDataBuilder()
				.buildCard(BUYER_NAME, 12345L, NEXT_DAY, 123).getCard();
	}

	/**
	 * @return cartão com titular nulo.
	 */
	protected Card createInvalidCard() {
		return new PaymentDataBuilder().buildCard(null, 12345L, NEXT_DAY, 123)
				.getCard();
	}

	/**
	 * @return pagamento válido, de acordo com tipo de pagamento.
	 */
	protected abstract Payment createValidPayment();

	/**
	 * @return pagamento inválido, de acordo com tipo de pagamento.
	 */
	protected abstract Payment createInvalidPayment();

	/**
	 * Verificações de testes que o pagamento não deve ser efetuado com sucesso.
	 * 
	 * @param paymentRecord
	 *            registro do pagamento.
	 * @param errorLocal
	 *            nome do local onde o erro foi encontrado,se campo do cliente,
	 *            do comprador, etc.
	 */
	protected void assertFailTest(PaymentRecord paymentRecord,
			String errorLocal) {
		String message = paymentRecord.getMessage();
		assertTrue(message, message.contains("Erro"));
		assertTrue(message, message.contains(errorLocal));
		assertFailStatus(paymentRecord.getStatus());
	}

	protected abstract void assertFailStatus(String status);

	/**
	 * Verificações de testes que o pagamento foi gerado e registrado com
	 * sucesso.
	 * 
	 * @param message
	 *            mensagem esperada.
	 * @param paymentRecord
	 *            dados do pagamento efetuado.
	 */
	protected void assertSuccessTest(String message,
			PaymentRecord paymentRecord) {
		assertEquals(message, paymentRecord.getMessage());
		assertNotNull(paymentRecord.getStatus());
		assertFalse(paymentRecord.getStatus().isEmpty());
	}
}