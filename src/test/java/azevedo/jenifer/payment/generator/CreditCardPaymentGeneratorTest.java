package azevedo.jenifer.payment.generator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import azevedo.jenifer.payment.model.Payment;
import azevedo.jenifer.payment.model.PaymentRecord;
import azevedo.jenifer.payment.model.PaymentType;
import azevedo.jenifer.payment.utils.PaymentDataBuilder;

/**
 * Testes para pagamento via cartão de crédito.
 * 
 * @author jenifer
 */
public class CreditCardPaymentGeneratorTest extends PaymentGeneratorTest {

	private static final long AMOUNT = 1000L;

	/**
	 * Verifica pagamento com todos os dados corretos.
	 */
	@Test
	public void testSuccessCreditPayment() {
		PaymentRecord paymentRecord = PaymentGenerator.generate(buildSuccess());
		assertSuccessTest(CreditCardPaymentGenerator.OK_MSG, paymentRecord);

		try {
			Boolean isOk = Boolean.valueOf(paymentRecord.getStatus());
			assertNotNull(isOk);
			assertTrue(isOk);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Verifica se pagamento é considerado inválido.</br>
	 * Pagamento com cliente inválido não pode ser efetuado com sucesso e deve
	 * conter mensagem apontando erro no cliente.
	 */
	@Test
	public void testInvalidClientCreditPayment() {
		PaymentRecord paymentRecord = PaymentGenerator
				.generate(buildFailInvalidClient());
		assertFailTest(paymentRecord, "Cliente");
	}

	/**
	 * Verifica se pagamento é considerado inválido.</br>
	 * Pagamento com comprador inválido não pode ser efetuado com sucesso e deve
	 * conter mensagem apontando erro no comprador.
	 */
	@Test
	public void testInvalidBuyerCreditPayment() {
		PaymentRecord paymentRecord = PaymentGenerator
				.generate(buildFailInvalidBuyer());
		assertFailTest(paymentRecord, "Comprador");
	}

	/**
	 * Verifica se pagamento é considerado inválido.</br>
	 * Pagamento com dados do pagamento inválido não pode ser efetuado com
	 * sucesso e deve conter mensagem apontando erro.
	 */
	@Test
	public void testInvalidPaymentCreditPayment() {
		PaymentRecord paymentRecord = PaymentGenerator
				.generate(buildFailInvalidPayment());
		assertFailTest(paymentRecord, "Pagamento");
	}

	/**
	 * Verifica se pagamento é considerado inválido.</br>
	 * Pagamento com cartão inválido não pode ser efetuado com sucesso e deve
	 * conter mensagem apontando erro no cartão.
	 */
	@Test
	public void testInvalidCardCreditPayment() {
		PaymentRecord paymentRecord = PaymentGenerator
				.generate(buildFailInvalidCard());
		assertFailTest(paymentRecord, "Cartão");
	}

	private PaymentRecord buildFailInvalidCard() {
		return buildBaseScenario().setPayment(createInvalidPaymentCard())
				.getPaymentRecord();
	}

	/**
	 * Cria pagamento com dados válidos.
	 */
	@Override
	protected Payment createValidPayment() {
		return new PaymentDataBuilder().buildPayment(AMOUNT,
				PaymentType.CREDIT_CARD, createValidCard()).getPayment();
	}

	/**
	 * Cria pagamento com dados inválidos.
	 */
	@Override
	protected Payment createInvalidPayment() {
		return new PaymentDataBuilder()
				.buildPayment(null, PaymentType.CREDIT_CARD, createValidCard())
				.getPayment();
	}

	/**
	 * Cria pagamento com cartão inválido.
	 */
	private Payment createInvalidPaymentCard() {
		return new PaymentDataBuilder().buildPayment(AMOUNT,
				PaymentType.CREDIT_CARD, createInvalidCard()).getPayment();
	}

	@Override
	protected void assertFailStatus(String status) {
		assertNotNull(status);
		assertFalse(Boolean.getBoolean(status));
	}

	private void assertFalse(boolean boolean1) {
		// TODO Auto-generated method stub

	}
}
