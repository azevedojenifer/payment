package azevedo.jenifer.payment.utils;

import java.util.Date;

import azevedo.jenifer.payment.model.Buyer;
import azevedo.jenifer.payment.model.Card;
import azevedo.jenifer.payment.model.Client;
import azevedo.jenifer.payment.model.Payment;
import azevedo.jenifer.payment.model.PaymentRecord;
import azevedo.jenifer.payment.model.PaymentType;

/**
 * Responsável por criar itens para registrar um pagamento.
 * 
 * @author jenifer
 */
public final class PaymentDataBuilder {

	private Client client;

	private Buyer buyer;

	private Card card;

	private Payment payment;

	public PaymentDataBuilder buildClient(String id) {
		this.client = new Client(id);
		return this;
	}

	public PaymentDataBuilder buildBuyer(String name, String email, Long cpf) {
		this.buyer = new Buyer(name, email, cpf);
		return this;
	}

	public PaymentDataBuilder buildCard(String cardHolderName, Long cardNumber,
			Date cardExpirationDate, Integer cardCVV) {
		this.card = new Card(cardHolderName, cardNumber, cardExpirationDate,
				cardCVV);
		return this;
	}

	public PaymentDataBuilder buildPayment(Long amount, PaymentType paymentType,
			Card card) {
		this.payment = new Payment(amount, paymentType, card);
		return this;
	}

	public Client getClient() {
		return client;
	}

	public PaymentDataBuilder setClient(Client client) {
		this.client = client;
		return this;
	}

	public Buyer getBuyer() {
		return buyer;
	}

	public PaymentDataBuilder setBuyer(Buyer buyer) {
		this.buyer = buyer;
		return this;
	}

	public Card getCard() {
		return card;
	}

	public PaymentDataBuilder setCard(Card card) {
		this.card = card;
		return this;
	}

	public Payment getPayment() {
		return payment;
	}

	public PaymentDataBuilder setPayment(Payment payment) {
		this.payment = payment;
		return this;
	}

	public PaymentRecord getPaymentRecord() {
		return new PaymentRecord(client, buyer, payment);
	}
}