package azevedo.jenifer.payment.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

import azevedo.jenifer.payment.checkers.PaymentChecker;
import azevedo.jenifer.payment.exception.PaymentException;
import azevedo.jenifer.payment.utils.PaymentDataBuilder;

public class PaymentTest {

	private static final long AMOUNT = 1000;

	private static final Date NOW = new Date();

	private static final Date DATE = new Date(NOW.getTime() + 1000);

	private static final Card VALID_CARD = createValidCard();

	/**
	 * Verifica se cartão com dados corretos é valido.
	 */
	@Test
	public void testValidPayment() {
		assertTrue(isValidPayment(AMOUNT, PaymentType.BOLETO, null));
		assertTrue(isValidPayment(AMOUNT, PaymentType.CREDIT_CARD, VALID_CARD));
	}

	/**
	 * Verifica se pagamento sem valor ou valor zerado é inválido.
	 */
	@Test
	public void testInvalidPaymentAmount() {
		assertFalse(isValidPayment(null, PaymentType.BOLETO, null));
		assertFalse(isValidPayment(0L, PaymentType.BOLETO, null));

		assertFalse(isValidPayment(null, PaymentType.CREDIT_CARD, VALID_CARD));
		assertFalse(isValidPayment(0L, PaymentType.CREDIT_CARD, VALID_CARD));
	}

	/**
	 * Verifica se pagamento sem tipo de pagamento, com ou sem cartão, é
	 * inválido.
	 */
	@Test
	public void testInvalidPayment() {
		assertFalse(isValidPayment(AMOUNT, null, null));
		assertFalse(isValidPayment(AMOUNT, null, VALID_CARD));
	}

	/**
	 * Verifica se tipo de pagamento não condizente com cartão é inválido.
	 */
	@Test
	public void testInvalidPaymentCard() {
		assertFalse(isValidPayment(AMOUNT, PaymentType.BOLETO, VALID_CARD));
		assertFalse(isValidPayment(AMOUNT, PaymentType.CREDIT_CARD, null));
	}

	/**
	 * Cria o pagamento e valida dados deste pagamento.
	 * 
	 * @param amount
	 *            valor
	 * @param paymentType
	 *            tipo do pagamento
	 * @param card
	 *            dados do cartão, se pagamento deste tipo.
	 * @return true se todos os dados do pagamento estão corretos; false caso
	 *         contrário.
	 */
	private boolean isValidPayment(Long amount, PaymentType paymentType,
			Card card) {
		try {
			Payment payment = new PaymentDataBuilder()
					.buildPayment(amount, paymentType, card).getPayment();
			PaymentChecker.check(payment);
			return true;
		} catch (PaymentException e) {
			return false;
		}
	}

	private static Card createValidCard() {
		return new PaymentDataBuilder()
				.buildCard("Holder Name", 11111L, DATE, 1).getCard();
	}
}