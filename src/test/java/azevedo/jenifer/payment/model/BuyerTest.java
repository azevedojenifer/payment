package azevedo.jenifer.payment.model;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import azevedo.jenifer.payment.checkers.BuyerChecker;
import azevedo.jenifer.payment.exception.BuyerException;
import azevedo.jenifer.payment.utils.PaymentDataBuilder;

public class BuyerTest {

	private static final String NAME = "Buyer Name Test";

	private static final String EMAIL = "buyer.name@test.com";

	private static final long CPF = 1111111111L;

	/**
	 * Verifica um comprador com todos os dados válidos.
	 * 
	 * @throws BuyerException
	 */
	@Test
	public void testValidBuyer() throws BuyerException {
		assertTrue(checkBuyer(NAME, EMAIL, CPF));
	}

	/**
	 * Verifica se comprador sem nome.
	 */
	@Test(expected = BuyerException.class)
	public void testNoBuyerName() throws BuyerException {
		checkBuyer(null, EMAIL, CPF);
	}

	/**
	 * Verifica se comprador com nome vazio.
	 */
	@Test(expected = BuyerException.class)
	public void testInvalidBuyerName() throws BuyerException {
		checkBuyer(" ", EMAIL, CPF);
	}

	/**
	 * Verifica se comprador sem email.
	 */
	@Test(expected = BuyerException.class)
	public void testNoBuyerEmail() throws BuyerException {
		checkBuyer(NAME, null, CPF);
	}

	/**
	 * Verifica se comprador com email vazio.
	 */
	@Test(expected = BuyerException.class)
	public void testInvalidBuyerEmail() throws BuyerException {
		checkBuyer(NAME, " ", CPF);
	}

	/**
	 * Verifica se comprador sem CPF.
	 * 
	 * @throws BuyerException
	 */
	@Test(expected = BuyerException.class)
	public void testNoBuyerCPF() throws BuyerException {
		checkBuyer(NAME, EMAIL, null);
	}

	/**
	 * Verifica se comprador com CPF inválido.
	 * 
	 * @throws BuyerException
	 */
	@Test(expected = BuyerException.class)
	public void testInvalidBuyerCPF() throws BuyerException {
		checkBuyer(NAME, EMAIL, 0L);
	}

	/**
	 * Cria comprador e verifica se todos os dados estão corretos.
	 * 
	 * @param name
	 * @param email
	 * @param cpf
	 * 
	 * @throws BuyerException
	 *             se algum dado inválido.
	 */
	private boolean checkBuyer(String name, String email, Long cpf)
			throws BuyerException {
		BuyerChecker.check(new PaymentDataBuilder().buildBuyer(name, email, cpf)
				.getBuyer());
		return true;
	}
}