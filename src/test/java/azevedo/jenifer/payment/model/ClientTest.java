package azevedo.jenifer.payment.model;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import azevedo.jenifer.payment.checkers.ClientChecker;
import azevedo.jenifer.payment.exception.ClientException;
import azevedo.jenifer.payment.utils.PaymentDataBuilder;

public class ClientTest {

	/**
	 * Verifica se o cliente é válido.
	 * 
	 * @throws ClientException
	 */
	@Test
	public void testValidClient() throws ClientException {
		assertTrue(checkClient("fje9495fjse44w"));
	}

	/**
	 * Verifica se cliente sem id é invalido.
	 * 
	 * @throws ClientException
	 */
	@Test(expected = ClientException.class)
	public void testInvalidClient() throws ClientException {
		checkClient(null);
	}

	/**
	 * Cria e valida dados do cliente.
	 * 
	 * @param id
	 * @throws ClientException
	 *             caso cliente inválido.
	 */
	private boolean checkClient(String id) throws ClientException {
		ClientChecker
				.check(new PaymentDataBuilder().buildClient(id).getClient());
		return true;
	}
}
