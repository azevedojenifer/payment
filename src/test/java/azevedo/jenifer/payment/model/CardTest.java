package azevedo.jenifer.payment.model;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

import azevedo.jenifer.payment.checkers.CardChecker;
import azevedo.jenifer.payment.exception.CardException;
import azevedo.jenifer.payment.utils.PaymentDataBuilder;

public class CardTest {

	private static final String NAME = "Card H. Name Teste";

	private static final long NUMBER = 11111111111L;

	private static final int CVV = 1;

	private static final Date NOW = new Date();

	private static final Date DATE = new Date(NOW.getTime() + 1000);

	private static final Date EXPIRED = new Date(NOW.getTime());

	/**
	 * Verifica se cartão preenchido corretamente é válido.
	 */
	@Test
	public void testValidCard() throws CardException {
		assertTrue(checkCard(NAME, NUMBER, DATE, CVV));
		assertTrue(checkCard(NAME, NUMBER, DATE, CVV));
	}

	/**
	 * Verifica se cartão sem nome.
	 */
	@Test(expected = CardException.class)
	public void testNoCardName() throws CardException {
		checkCard(null, NUMBER, DATE, CVV);
	}

	/**
	 * Verifica se cartão com nome vazio.
	 */
	@Test(expected = CardException.class)
	public void testEmptyCardName() throws CardException {
		checkCard(" ", NUMBER, DATE, CVV);
	}

	/**
	 * Verifica se cartão sem número.
	 */
	@Test(expected = CardException.class)
	public void testNoCardNumber() throws CardException {
		checkCard(NAME, null, DATE, CVV);
	}

	/**
	 * Verifica se cartão com número invalido.
	 */
	@Test(expected = CardException.class)
	public void testInvalidCardNumber() throws CardException {
		checkCard(NAME, 0L, DATE, CVV);
	}

	/**
	 * Verifica se cartão sem cvv.
	 */
	@Test(expected = CardException.class)
	public void testNoCardCVV() throws CardException {
		checkCard(NAME, NUMBER, DATE, null);
	}

	/**
	 * Verifica se cartão com cvv invalido.
	 */
	@Test(expected = CardException.class)
	public void testInvalidCardCVV() throws CardException {
		checkCard(NAME, NUMBER, DATE, 0);
	}

	/**
	 * Verifica se cartão sem data de vencimento.
	 */
	@Test(expected = CardException.class)
	public void testNoCardExpiration() throws CardException {
		checkCard(NAME, NUMBER, null, CVV);
	}

	/**
	 * Verifica se cartão vencido.
	 */
	@Test(expected = CardException.class)
	public void testInvalidCardExpiration() throws CardException {
		checkCard(NAME, NUMBER, EXPIRED, CVV);
	}

	/**
	 * Cria cartão e verifica se todos os dados estão corretos.
	 * 
	 * @param cardHolderName
	 * @param cardNumber
	 * @param cardExpirationDate
	 * @param cardCVV
	 * 
	 * @throws CardException
	 *             se algum campo inválido.
	 */
	private boolean checkCard(String cardHolderName, Long cardNumber,
			Date cardExpirationDate, Integer cardCVV) throws CardException {
		CardChecker.check(new PaymentDataBuilder().buildCard(cardHolderName,
				cardNumber, cardExpirationDate, cardCVV).getCard(), NOW);
		return true;
	}
}