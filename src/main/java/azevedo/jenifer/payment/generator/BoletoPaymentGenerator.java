package azevedo.jenifer.payment.generator;

import java.util.Random;

import azevedo.jenifer.payment.model.PaymentRecord;

/**
 * Verifica e gera pagamento via boleto.
 * 
 * @author jenifer
 */
public class BoletoPaymentGenerator extends PaymentGenerator {

	private static final Random RAND = new Random();

	public static final String OK_MSG = "Pagamento via boleto gerado com sucesso";

	/**
	 * Verifica se este pagamento pode ser confirmado.<br>
	 * Gera o número do boleto e a mensagem.
	 * 
	 * @param paymentRecord
	 */
	@Override
	protected PaymentRecord generatePaymentStatus(PaymentRecord paymentRecord) {
		if (isValid(paymentRecord)) {
			String status = Long.toString(Math.abs(RAND.nextLong()) + 1L);
			paymentRecord.setStatus(status);
			paymentRecord.setMessage(OK_MSG);
		}
		return paymentRecord;
	}
}
