package azevedo.jenifer.payment.generator;

import azevedo.jenifer.payment.checkers.BuyerChecker;
import azevedo.jenifer.payment.checkers.ClientChecker;
import azevedo.jenifer.payment.checkers.PaymentChecker;
import azevedo.jenifer.payment.exception.PaymentAPIException;
import azevedo.jenifer.payment.model.Payment;
import azevedo.jenifer.payment.model.PaymentRecord;
import azevedo.jenifer.payment.model.PaymentType;

/**
 * Verifica e gera pagamento.
 * 
 * @author jenifer
 */
public abstract class PaymentGenerator {

	/**
	 * Verifica e gera o pagamento com as informações passadas.
	 * 
	 * @param client
	 *            dados do cliente.
	 * @param buyer
	 *            dados do comprador.
	 * @param payment
	 *            dados do pagamento.
	 * @return pagamento efetuado com status para identificar se válido ou não.
	 */
	public static PaymentRecord generate(PaymentRecord paymentRecord) {
		Payment payment = paymentRecord.getPayment();
		PaymentGenerator generator = getInstanceGeneratorOf(payment);
		return generator.generatePaymentStatus(paymentRecord);
	}

	/**
	 * Verifica se pagamento válido além de gerar informações sobre o status.
	 * 
	 * @param paymentRecord
	 *            pagamento efetuado.
	 * @return pagamento efetuado com status para identificar se válido ou não.
	 */
	protected abstract PaymentRecord generatePaymentStatus(
			PaymentRecord paymentRecord);

	protected boolean isValid(PaymentRecord paymentRecord) {
		try {
			ClientChecker.check(paymentRecord.getClient());
			BuyerChecker.check(paymentRecord.getBuyer());
			PaymentChecker.check(paymentRecord.getPayment());
			return true;
		} catch (PaymentAPIException e) {
			paymentRecord.setMessage(e.getMessage());
			return false;
		}
	}

	private static PaymentGenerator getInstanceGeneratorOf(Payment payment) {
		PaymentType type = payment == null ? null : payment.getPaymentType();
		if (type == PaymentType.CREDIT_CARD) {
			return new CreditCardPaymentGenerator();
		}
		return new BoletoPaymentGenerator();
	}
}