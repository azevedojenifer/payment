package azevedo.jenifer.payment.generator;

import java.util.Date;

import azevedo.jenifer.payment.checkers.CardChecker;
import azevedo.jenifer.payment.exception.PaymentAPIException;
import azevedo.jenifer.payment.model.PaymentRecord;

/**
 * Verifica e gera pagamento via cartão de crédito.
 * 
 * @author jenifer
 */
public class CreditCardPaymentGenerator extends PaymentGenerator {

	private static final String OK = "true";

	private static final String NOT_OK = "false";

	public static final String OK_MSG = "Pagamento via cartão efetuado com sucesso";

	/**
	 * Verifica se este pagamento pode ser confirmado.<br>
	 * Gera o número do cartão de crédito e a mensagem.
	 * 
	 * @param paymentRecord
	 */
	@Override
	protected PaymentRecord generatePaymentStatus(PaymentRecord paymentRecord) {
		if (isValid(paymentRecord) && isValidCard(paymentRecord)) {
			paymentRecord.setStatus(OK);
			paymentRecord.setMessage(OK_MSG);
		} else {
			paymentRecord.setStatus(NOT_OK);
		}
		return paymentRecord;
	}

	/**
	 * Verifica se os dados do cartão estão corretos.
	 * 
	 * @param paymentRecord
	 *            contém os dados do cartão.
	 * @return true cartão passado corretamente; false caso contrário.
	 */
	private boolean isValidCard(PaymentRecord paymentRecord) {
		try {
			CardChecker.check(paymentRecord.getCard(), new Date());
			return true;
		} catch (PaymentAPIException e) {
			paymentRecord.setMessage(e.getMessage());
			return false;
		}
	}
}
