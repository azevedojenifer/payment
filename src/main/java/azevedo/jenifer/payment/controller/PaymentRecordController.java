package azevedo.jenifer.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import azevedo.jenifer.payment.model.PaymentRecord;
import azevedo.jenifer.payment.repository.PaymentRepository;
import azevedo.jenifer.payment.service.PaymentService;

@RestController
public class PaymentRecordController {

	@Autowired
	private PaymentRepository paymentRepository;

	@RequestMapping(value = "/pagamentos/{id}", method = RequestMethod.GET)
	public PaymentRecord findById(@PathVariable(value = "id") String id) {
		return new PaymentService(paymentRepository).findPayment(id);
	}

	@RequestMapping(value = "/pagamentos", method = RequestMethod.POST)
	public String create(@RequestBody PaymentRecord paymentRecord) {
		return new PaymentService(paymentRepository)
				.recordPayment(paymentRecord);
	}
}