package azevedo.jenifer.payment.exception;

import azevedo.jenifer.payment.model.PaymentType;

/**
 * Exceção ao verificar um pagamento.
 * 
 * @author jenifer
 */
public class PaymentException extends PaymentAPIException {

	private static final long serialVersionUID = -283121520524287066L;

	private static final String NAME = "Pagamento";

	public PaymentException() {
		super(NAME);
	}

	public PaymentException(String field) {
		super(NAME, field);
	}

	public PaymentException(PaymentType type) {
		super(String.format(NAME + " de tipo (%s) inválido.", type));
	}
}
