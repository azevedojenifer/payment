package azevedo.jenifer.payment.exception;

/**
 * Exceção ao verificar comprador.
 * 
 * @author jenifer
 */
public class BuyerException extends PaymentAPIException {

	private static final long serialVersionUID = 3576647765097309684L;

	private static final String NAME = "Comprador";

	public BuyerException() {
		super(NAME);
	}

	public BuyerException(String field) {
		super(NAME, field);
	}
}
