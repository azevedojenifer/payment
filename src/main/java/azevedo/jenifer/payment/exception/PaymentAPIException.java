package azevedo.jenifer.payment.exception;

public abstract class PaymentAPIException extends Exception {

	private static final long serialVersionUID = -5050949375597335826L;

	private static final String NULL_ITEM = "Erro: %s não informado.";

	private static final String NULL_FIELD = "Erro: Campo '%s' (%s) inválido ou não informado.";

	protected PaymentAPIException(String name) {
		super(String.format(NULL_ITEM, name));
	}

	protected PaymentAPIException(String name, String field) {
		super(String.format(NULL_FIELD, field, name));
	}
}
