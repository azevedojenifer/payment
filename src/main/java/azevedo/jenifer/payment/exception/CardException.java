package azevedo.jenifer.payment.exception;

/**
 * Exceção ao verificar um cartão de crédito.
 * 
 * @author jenifer
 */
public class CardException extends PaymentAPIException {

	private static final long serialVersionUID = 4477977528008136080L;

	private static final String NAME = "Cartão";

	public CardException() {
		super(NAME);
	}

	public CardException(String field) {
		super(NAME, field);
	}
}
