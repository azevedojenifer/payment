package azevedo.jenifer.payment.exception;

/**
 * Exceção ao verificar um cliente.
 * 
 * @author jenifer
 */
public class ClientException extends PaymentAPIException {

	private static final long serialVersionUID = 8945351003292946453L;

	private static final String NAME = "Cliente";

	public ClientException() {
		super(NAME);
	}

	public ClientException(String field) {
		super(NAME, field);
	}
}
