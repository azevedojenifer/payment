package azevedo.jenifer.payment.model;

/**
 * Informações do pagamento a ser registrado.
 * 
 * @author jenifer
 */
public class Payment {

	private Long amount;

	private PaymentType paymentType;

	private Card card;

	public Payment() {
	}

	public Payment(Long amount, PaymentType paymentType, Card card) {
		this.amount = amount;
		this.paymentType = paymentType;
		this.card = card;
	}

	public Long getAmount() {
		return this.amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public PaymentType getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Card getCard() {
		return this.card;
	}

	public void setCard(Card card) {
		this.card = card;
	}
}
