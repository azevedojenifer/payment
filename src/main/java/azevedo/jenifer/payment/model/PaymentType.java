package azevedo.jenifer.payment.model;

public enum PaymentType {

	BOLETO("Boleto"), CREDIT_CARD("Cartão de Cédito");

	private final String label;

	private PaymentType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
