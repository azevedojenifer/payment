package azevedo.jenifer.payment.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Registro do pagamento efetuado.
 * 
 * @author jenifer
 *
 */
@Document(collection = "payments")
public class PaymentRecord {

	@Id
	private String id;

	/* Cliente */

	private String clientId;

	/* Comprador */

	private String name;

	private String email;

	private Long cpf;

	/* Pagamento */

	private Long amount;

	private PaymentType paymentType;

	/* Cartão */

	private Date cardExpirationDate;

	private String cardHolderName;

	private Long cardNumber;

	private Integer cardCVV;

	/* Informações do registo do pagamento */

	private String status;

	private String message;

	public PaymentRecord() {
	}

	public PaymentRecord(Client client, Buyer buyer, Payment payment) {
		this.clientId = client.getId();

		this.name = buyer.getName();
		this.email = buyer.getEmail();
		this.cpf = buyer.getCpf();

		this.amount = payment.getAmount();
		this.paymentType = payment.getPaymentType();

		Card card = payment.getCard();
		if (card != null) {
			this.cardHolderName = card.getCardHolderName();
			this.cardNumber = card.getCardNumber();
			this.cardExpirationDate = card.getCardExpirationDate();
			this.cardCVV = card.getCardCVV();
		}
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCpf() {
		return this.cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public Long getAmount() {
		return this.amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public PaymentType getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Date getCardExpirationDate() {
		return this.cardExpirationDate;
	}

	public void setCardExpirationDate(Date cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}

	public String getCardHolderName() {
		return this.cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public Long getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getCardCVV() {
		return this.cardCVV;
	}

	public void setCardCVV(Integer cardCVV) {
		this.cardCVV = cardCVV;
	}

	public String getStatus() {
		return this.status;
	}

	public PaymentRecord setStatus(String status) {
		this.status = status;
		return this;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Pagamento[");
		sb.append("id=").append(this.getId()).append(", ");
		sb.append("status=").append(this.getStatus());
		return super.toString();
	}

	public Client getClient() {
		return new Client(clientId);
	}

	public Buyer getBuyer() {
		return new Buyer(name, email, cpf);
	}

	public Payment getPayment() {
		Card card = (paymentType == PaymentType.BOLETO) ? null : getCard();
		return new Payment(amount, paymentType, card);
	}

	public Card getCard() {
		return new Card(cardHolderName, cardNumber, cardExpirationDate,
				cardCVV);
	}
}