package azevedo.jenifer.payment.model;

import java.util.Date;

/**
 * Cartão utilizado no pagamento de tipo CREDIT_CARD.
 * 
 * @author jenifer
 */
public class Card {

	private Date cardExpirationDate;

	private String cardHolderName;

	private Long cardNumber;

	private Integer cardCVV;

	public Card() {
	}

	public Card(String cardHolderName, Long cardNumber, Date cardExpirationDate,
			Integer cardCVV) {
		this.cardExpirationDate = cardExpirationDate;
		this.cardHolderName = cardHolderName;
		this.cardNumber = cardNumber;
		this.cardCVV = cardCVV;
	}

	public Date getCardExpirationDate() {
		return this.cardExpirationDate;
	}

	public void setCardExpirationDate(Date cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}

	public String getCardHolderName() {
		return this.cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public Long getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getCardCVV() {
		return this.cardCVV;
	}

	public void setCardCVV(Integer cardCVV) {
		this.cardCVV = cardCVV;
	}
}