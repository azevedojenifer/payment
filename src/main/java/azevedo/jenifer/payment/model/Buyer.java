package azevedo.jenifer.payment.model;

/**
 * Comprador do pagamento.
 * 
 * @author jenifer
 */
public class Buyer {

	private String name;

	private String email;

	private Long cpf;

	public Buyer() {
	}

	public Buyer(String name, String email, Long cpf) {
		this.name = name;
		this.email = email;
		this.cpf = cpf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}
}