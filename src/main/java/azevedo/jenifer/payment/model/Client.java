package azevedo.jenifer.payment.model;

/**
 * Cliente de um pagamento.
 * 
 * @author jenifer
 */
public class Client {

	private String id;

	public Client() {
	}

	public Client(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}