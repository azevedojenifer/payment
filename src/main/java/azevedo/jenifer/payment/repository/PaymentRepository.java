package azevedo.jenifer.payment.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import azevedo.jenifer.payment.model.PaymentRecord;

public interface PaymentRepository
		extends MongoRepository<PaymentRecord, String> {
}
