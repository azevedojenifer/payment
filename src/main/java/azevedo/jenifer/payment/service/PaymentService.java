package azevedo.jenifer.payment.service;

import java.util.Collections;

import azevedo.jenifer.payment.generator.PaymentGenerator;
import azevedo.jenifer.payment.model.PaymentRecord;
import azevedo.jenifer.payment.repository.PaymentRepository;

/**
 * Responsável por gerenciar e prover os recursos fornecidos pela API.
 * 
 * @author jenifer
 *
 */
public class PaymentService {

	private final PaymentRepository paymentRepository;

	public PaymentService(PaymentRepository paymentRepository) {
		this.paymentRepository = paymentRepository;
	}

	/**
	 * Gera e salva no banco de dados o registro de um pedido de pagamento.
	 * 
	 * @param paymentRecord
	 *            dados do pagamento a serem salvos no banco com informação do
	 *            status.
	 * @return status sobre o resultado deste pagamento.
	 */
	public String recordPayment(PaymentRecord paymentRecord) {
		try {
			paymentRecord = PaymentGenerator.generate(paymentRecord);
			paymentRepository.save(paymentRecord);
			return paymentRecord.getStatus();
		} catch (Exception e) {
			return "Erro ao salvar dados: " + e.getMessage();
		}
	}

	public PaymentRecord findPayment(String id) {
		Iterable<PaymentRecord> payments = paymentRepository
				.findAllById(Collections.singleton(id));
		if (payments == null) {
			return new PaymentRecord()
					.setStatus("Nenhum item encontrado com este ID");
		}

		for (PaymentRecord paymentRecord : payments) {
			return paymentRecord;
		}

		return new PaymentRecord()
				.setStatus("Nenhum item encontrado com este ID");
	}
}