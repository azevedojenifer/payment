package azevedo.jenifer.payment.checkers;

import java.util.Date;

/**
 * Faz as verificações de campos básicos.
 * 
 * @author jenifer
 *
 */
public final class Checker {

	private Checker() {
	}

	protected static boolean isValid(Long value) {
		return value != null && value > 0L;
	}

	protected static boolean isValid(String value) {
		return value != null && !value.trim().isEmpty();
	}

	protected static boolean isValid(Date toValidate, Date date) {
		return toValidate != null && toValidate.getTime() > date.getTime();
	}

	protected static boolean isValid(Integer value) {
		return value != null && value > 0;
	}

	protected static boolean isValid(Double value) {
		return value != null && Math.abs(value) > 0.0;
	}
}