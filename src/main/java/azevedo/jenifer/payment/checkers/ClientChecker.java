package azevedo.jenifer.payment.checkers;

import azevedo.jenifer.payment.exception.ClientException;
import azevedo.jenifer.payment.model.Client;

/**
 * Verifica se informações do cliente estão corretas.
 * 
 * @author jenifer
 */
public class ClientChecker {

	/**
	 * Verifica se este cliente foi informado corretamente.
	 * 
	 * @param client
	 * @throws ClientException
	 *             caso cliente nulo ou com informações inválidas.
	 */
	public static void check(Client client) throws ClientException {
		if (client == null) {
			throw new ClientException();
		}
		if (!Checker.isValid(client.getId())) {
			throw new ClientException("ID");
		}
	}
}
