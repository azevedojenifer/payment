package azevedo.jenifer.payment.checkers;

import azevedo.jenifer.payment.exception.PaymentException;
import azevedo.jenifer.payment.model.Card;
import azevedo.jenifer.payment.model.Payment;
import azevedo.jenifer.payment.model.PaymentType;

/**
 * Verifica se dados do pagamento estão corretos.
 * 
 * @author jenifer
 */
public class PaymentChecker {

	/**
	 * Verifica se informações deste pagamento estão corretas.
	 * 
	 * @param payment
	 *            dados do pagamento
	 * @param paymentDate
	 *            data do pagamento
	 * @throws PaymentException
	 *             caso pagamento nulo ou dados inválidos.S
	 */
	public static void check(Payment payment) throws PaymentException {
		if (payment == null) {
			throw new PaymentException();
		}
		if (!Checker.isValid(payment.getAmount())) {
			throw new PaymentException("Valor");
		}
		PaymentType type = payment.getPaymentType();
		if (type == null) {
			throw new PaymentException("Tipo de pagamento");
		}
		checkTypeAndCard(payment.getCard(), type);
	}

	/**
	 * Verifica se dados do cartão está de acordo com tipo do pagamento.
	 * 
	 * @param card
	 * @param type
	 * @throws PaymentException
	 */
	private static void checkTypeAndCard(Card card, PaymentType type)
			throws PaymentException {
		if (type == PaymentType.BOLETO && card != null) {
			throw new PaymentException("Cartão");
		}
		if (type == PaymentType.CREDIT_CARD && card == null) {
			throw new PaymentException("Cartão");
		}
	}
}
