package azevedo.jenifer.payment.checkers;

import java.util.Date;

import azevedo.jenifer.payment.exception.CardException;
import azevedo.jenifer.payment.model.Card;

/**
 * Verifica se informações do cartão estão corretas.
 * 
 * @author jenifer
 */
public class CardChecker {

	/**
	 * Verifica os campos deste cartão.
	 * 
	 * @param card
	 *            cartão
	 * @param buyingDate
	 *            data da compra com este cartão.
	 * @throws CardException
	 *             caso cartão nulo ou com alguma informação invalida.
	 */
	public static void check(Card card, Date buyingDate) throws CardException {
		if (card == null) {
			throw new CardException();
		}
		if (!Checker.isValid(card.getCardExpirationDate(), buyingDate)) {
			throw new CardException("Vencimento");
		}
		if (!Checker.isValid(card.getCardHolderName())) {
			throw new CardException("Titular");
		}
		if (!Checker.isValid(card.getCardNumber())) {
			throw new CardException("Número");
		}
		if (!Checker.isValid(card.getCardCVV())) {
			throw new CardException("CVV");
		}
	}
}
