package azevedo.jenifer.payment.checkers;

import azevedo.jenifer.payment.exception.BuyerException;
import azevedo.jenifer.payment.model.Buyer;

/**
 * Verifica se as informações de um comprador estão corretas.
 * 
 * @author jenifer
 */
public class BuyerChecker {

	/**
	 * Verifica os campos deste comprador.
	 * 
	 * @param buyer
	 *            comprador
	 * @throws BuyerException
	 *             caso o comprador seja nulo ou um dos campos inválidos.
	 */
	public static void check(Buyer buyer) throws BuyerException {
		if (buyer == null) {
			throw new BuyerException();
		}
		if (!Checker.isValid(buyer.getName())) {
			throw new BuyerException("Nome");
		}
		if (!Checker.isValid(buyer.getEmail())) {
			throw new BuyerException("Email");
		}
		if (!Checker.isValid(buyer.getCpf())) {
			throw new BuyerException("CPF");
		}
	}
}
